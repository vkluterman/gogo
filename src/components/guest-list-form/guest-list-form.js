import React, {Component} from 'react';
import './guest-list-form.css';

export default class GuestListForm extends Component {

    state = {
     listStyle: "list-group formStyle"
    };
    
onSubmit = (e) => {
e.preventDefault();
this.props.onItemAdded(this.state.label);


};
 
onLabelChange = (e) => {
    this.setState({
     label: e.target.value
    });
};

render () {
console.log(this.props);

    return (
    <form className={this.state.listStyle} 
            onSubmit={this.onSubmit}> 

        <div className="input-group">
        <input 
            placeholder="Сообщение" 
            type="text" 
            className="name form-control"
            onChange={this.onLabelChange} 
        />
        <div className="input-group-append">
        <button 
        className="guestButton btn btn-info" 
        >
        Отправить 
            </button>
            </div>
        </div>  
    </form>
        );
    };
};